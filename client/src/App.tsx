import * as React from 'react';
import './App.css';
import * as fb from 'fabric';
declare var fabric: typeof fb.fabric;
const logo = require('./logo.svg');
const wood=require('./wood.jpg');
class App extends React.Component {
  canvas: HTMLCanvasElement;
  componentDidMount() {
    this.canvas.width = 300;
    this.canvas.height = 300;
    var ctx: CanvasRenderingContext2D | null = this.canvas.getContext("2d");
    if (ctx == null) {
      return;
    }
    else {
      let canvas = new fabric.Canvas(this.canvas,{
        selection:false,        
        interactive:false,
        containerClass:'game'
      });
      let ctx=this.canvas.getContext('2d');
      canvas.on('mouse:out',(e)=>{
        canvas.renderAll(true);
      });
      
      var circle = new fabric.Circle({
        fill:'url("http://localhost:7000/static/wood.jpg")',
        radius: 40,
        stroke: 'black',
        strokeWidth: 2,
      });
      var circle2=new fabric.Circle({
        fill:'rgba(0, 2, 0, 0)',
        opacity:1,
        stroke:'red',
        top:4,
        left:4,
        radius:36,
        strokeWidth:2
      });
      
     
      var text=new fabric.Text('車',{
        width:30,
        height:30,
        fill:'red',
        stroke:'red'
      });
      text.set({
        left:20,
        top:20,
       });
       
      var swamp=new fabric.Group([circle,circle2,text],{  
        top:10,
        left:10,
       lockMovementX:true,
       lockMovementY:true ,
       lockScalingX:true,
       lockScalingY:true,
       lockRotation:true,
       shadow:'black 0px 0px 12px'

      });
      fabric.util.loadImage('/static/wood.jpg',(e)=>{
        circle.set('fill',new fabric.Pattern({
        source:e,
        repeat:'repeat',
        offsetX:0,
        offsetY:0
        }));
        canvas.renderAll();
      });

      swamp.on('mousedown',(e)=>{
        console.log('down');
        swamp.setShadow('blue 0px 0px 12px');
        canvas.renderAll();
      });
      swamp.on('mouseup',(e)=>{
        swamp.setShadow('black 0px 0px 12px');
        console.log('up');
        canvas.renderAll()
      });
      canvas.add(swamp);
    }
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React test hello,world</h1>
        </header>
        <div style={{margin:"0 auto",position:'relative'}}>
          <canvas style={{margin: "0 auto"}} id="game" ref={(canvas: HTMLCanvasElement) => this.canvas = canvas} />
        </div>
      </div>
    );
  }
}

export default App;
