process.env.NODE_ENV = 'development';
var path=require('path');
var gulpWebpack=require('webpack-stream');
var webpack=require('webpack');
var ts=require('gulp-typescript');
var sourcemaps=require('gulp-sourcemaps');
const chalk = require('chalk');
var tsServerProject=ts.createProject('server/tsconfig.json');
var config=require('./client/config/webpack.config.dev');
const formatWebpackMessages = require('react-dev-utils/formatWebpackMessages');
let spawn = require('child_process').spawn,
node;

var browserSync=require('browser-sync');

var gulp=require('gulp');
config.output.path=path.join(__dirname,"build/client");
gulp.task('build-client-dev',()=>{
  return gulpWebpack(config).pipe(gulp.dest('build/client'));
})

gulp.task('build-client', (done)=>{
  webpack(config).run((err,stats)=>{
    if(err){
      done(err);
    }
    else{
      console.log("Compile client complete");
      done()
    }
  });

});

gulp.task('build-server',()=>{
   return tsServerProject.src()
  .pipe(sourcemaps.init())
      .pipe(tsServerProject())
   .pipe(sourcemaps.write('.',{     
     sourceRoot:'../server',
     includeContent:false    
   }))
      .pipe(gulp.dest('build'));
});

gulp.task("watch-client",()=>{
  gulp.run('build-client');
  console.log('Watch client');
  gulp.watch(['client/**/*'],()=>{
      gulp.run('build-client');
  });
})

gulp.task('server',['build-server'],()=>{
  if(node)
  node.kill();
  node=spawn('node',['build/app.js'],{stdio:'inherit'});
  node.on('close',(code)=>{
        if(code==8){
          gulp.log('Error while running, waiting for changes');
        }
  });
});

gulp.task('watch-server',()=>{
  let browser=browserSync.init({
proxy:'http://localhost:3000',
files:'build/**/*',
browser:'google chrome',
port:7000
  });
  gulp.run('server');
  console.log('Watch server');
  gulp.watch(['server/**/*.ts'],()=>{
    gulp.run('server');
   // browser.reload();
  })
});

gulp.task("watch",['watch-client','watch-server'],()=>{

});
gulp.task('default',['build-client','build-server']);

