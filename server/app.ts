import * as express from 'express';
import * as path from 'path';
let app=express();
app.get('/',(req,res)=>{
  res.redirect('/index.html');
});
app.use(express.static(path.join(__dirname,'client')));
app.listen(3000,()=>{
  console.log("Listening on port 3000");
});